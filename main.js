/* eslint no-console:0 consistent-return:0 */
"use strict";

function createShader(gl, type, source) {
  var shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  }

  console.log(gl.getShaderInfoLog(shader));
  gl.deleteShader(shader);
}

function createProgram(gl, vertexShader, fragmentShader) {
  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  var success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  }

  console.log(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);
}

function pointToMatrix(point) {
  var result = [point[0], point[1], 0, 1,
                0, 0, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 0];

  return result;
}


function main() {
  // Get A WebGL context
  var canvas = document.querySelector("#canvas");
  var gl = canvas.getContext("webgl");
  if (!gl) {
    return;
  }

  // Get the strings for our GLSL shaders
  var vertexShaderSource = document.querySelector("#vertex-shader-2d-reflection").text;
  var fragmentShaderSource = document.querySelector("#fragment-shader-2d").text;

  // create GLSL shaders, upload the GLSL source, compile the shaders
  var vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  var fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);

  // Link the two shaders into a program
  var program = createProgram(gl, vertexShader, fragmentShader);

  // look up where the vertex data needs to go.
  var positionAttributeLocation = gl.getAttribLocation(program, "a_position");

  // Create a buffer and put three 2d clip space points in it
  var positionBuffer = gl.createBuffer();

  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  var positions = [
    0.2, 0.0,
    -0.5, -0.5,
    -0.5, 0.5
  ];
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
  var refection_point_loc = gl.getUniformLocation(program, "u_refection_point");

  var reflectionPoint = [0.5, 0];
  var reflectionPointM = pointToMatrix(reflectionPoint);


  //
  // Translations
  //
  // Get the strings for our GLSL shaders
  var vertexShaderSourceTrans = document.querySelector("#vertex-shader-2d-translation").text;
  var fragmentShaderSource = document.querySelector("#fragment-shader-2d").text;

  // create GLSL shaders, upload the GLSL source, compile the shaders
  var vertexShaderTrans = createShader(gl, gl.VERTEX_SHADER, vertexShaderSourceTrans);
  var fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);

  // Link the two shaders into a program
  var programTrans = createProgram(gl, vertexShaderTrans, fragmentShader);

  // look up where the vertex data needs to go.
  var positionAttributeLocationTrans = gl.getAttribLocation(programTrans, "a_position");

  // Create a buffer and put three 2d clip space points in it
  var positionBufferTrans = gl.createBuffer();

  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferTrans);

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  var point_a_loc = gl.getUniformLocation(programTrans, "u_point_a");
  var point_b_loc = gl.getUniformLocation(programTrans, "u_point_b");

  var pointA = [0,0];
  var pointB = [0.0, 0];


  var pointAM = pointToMatrix(pointA);
  var pointBM = pointToMatrix(pointB);

  //
  // The Second shader
  //


  var vertexShaderSourcePlain = document.querySelector("#vertex-shader-2d-plain").text;
  var fragmentShaderSource = document.querySelector("#fragment-shader-2d-plain").text;

  var vertexShaderPlain= createShader(gl, gl.VERTEX_SHADER, vertexShaderSourcePlain);
  var fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);

  var programPlain = createProgram(gl, vertexShaderPlain, fragmentShader);

  var positionAttributeLocationPlain = gl.getAttribLocation(programPlain, "a_position");

  var positionBufferPlain = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferPlain);

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);


  //
  // Third Shader
  //



  var vertexShaderSourcePoint = document.querySelector("#vertex-shader-2d-point").text;
  var fragmentShaderSourcePoint = document.querySelector("#fragment-shader-2d-point").text;

  var vertexShaderPoint= createShader(gl, gl.VERTEX_SHADER, vertexShaderSourcePoint);
  var fragmentShaderPoint = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSourcePoint);

  var programPoint = createProgram(gl, vertexShaderPoint, fragmentShaderPoint);

  var positionAttributeLocationPoint = gl.getAttribLocation(programPoint, "a_position");

  var positionBufferPoint = gl.createBuffer();


  //
  // Fourth Shader
  //

  var vertexShaderSourceCircle = document.querySelector("#vertex-shader-2d-circle").text;
  var fragmentShaderSourceCircle = document.querySelector("#fragment-shader-2d-circle").text;

  var vertexShaderCircle= createShader(gl, gl.VERTEX_SHADER, vertexShaderSourceCircle);
  var fragmentShaderCircle = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSourceCircle);

  var programCircle = createProgram(gl, vertexShaderCircle, fragmentShaderCircle);

  var positionAttributeLocationCircle = gl.getAttribLocation(programCircle, "a_position");

  var positionBufferCircle = gl.createBuffer();

  var circle = {x: 0, y: 0, r: 1};
  var ATTRIBUTES = 2;
  var numFans = 100;
  var degreePerFan = (2 * Math.PI) / numFans;
  var vertexData = [circle.x, circle.y];

  for(var i = 0; i <= numFans; i++) {
    var index = ATTRIBUTES * i + 0; // there is already 2 items in array
    var angle = degreePerFan * (i+1);
    vertexData[index] = circle.x + Math.cos(angle) * circle.r;
    vertexData[index + 1] = circle.y + Math.sin(angle) * circle.r;
  }

  var vertexDataTyped = new Float32Array(vertexData);


  function render() {

    if (isReflection) {
      var refection_point_loc = gl.getUniformLocation(program, "u_refection_point");
      var reflectionPointM = pointToMatrix(reflectionPoint);
      // console.log(reflectionPointM);
      // code below this line is rendering code.

      webglUtils.resizeCanvasToDisplaySize(gl.canvas);

      // Tell WebGL how to convert from clip space to pixels
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      // Clear the canvas
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);

      // Tell it to use our program (pair of shaders)
      gl.useProgram(program);

      // Turn on the attribute
      gl.enableVertexAttribArray(positionAttributeLocation);

      // Bind the position buffer.
      gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

      // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
      var size = 2;          // 2 components per iteration
      var type = gl.FLOAT;   // the data is 32bit floats
      var normalize = false; // don't normalize the data
      var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
      var offset = 0;        // start at the beginning of the buffer
      gl.vertexAttribPointer(
        positionAttributeLocation, size, type, normalize, stride, offset);


      gl.uniformMatrix4fv(refection_point_loc, false, new Float32Array(reflectionPointM));

      // draw
      var primitiveType = gl.TRIANGLES;
      var offset = 0;
      var count = 3;
      gl.drawArrays(primitiveType, offset, count);

      // 2nd
      webglUtils.resizeCanvasToDisplaySize(gl.canvas);
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      // Clear the canvas
      // gl.clearColor(0, 0, 0, 0);
      // gl.clear(gl.COLOR_BUFFER_BIT);

      gl.useProgram(programPlain);
      gl.enableVertexAttribArray(positionAttributeLocationPlain);

      // Bind the position buffer.
      gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferPlain);

      // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
      var size = 2;          // 2 components per iteration
      var type = gl.FLOAT;   // the data is 32bit floats
      var normalize = false; // don't normalize the data
      var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
      var offset = 0;        // start at the beginning of the buffer
      gl.vertexAttribPointer(
        positionAttributeLocationPlain, size, type, normalize, stride, offset);

      // draw
      var primitiveType = gl.TRIANGLES;
      var offset = 0;
      var count = 3;
      gl.drawArrays(primitiveType, offset, count);

      // 3rd

      gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferPoint);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(reflectionPoint), gl.STATIC_DRAW);
      webglUtils.resizeCanvasToDisplaySize(gl.canvas);
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      // Clear the canvas
      // gl.clearColor(0, 0, 0, 0);
      // gl.clear(gl.COLOR_BUFFER_BIT);

      gl.useProgram(programPoint);
      gl.enableVertexAttribArray(positionAttributeLocationPoint);

      // Bind the position buffer.
      gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferPoint);

      // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
      var size = 2;          // 2 components per iteration
      var type = gl.FLOAT;   // the data is 32bit floats
      var normalize = false; // don't normalize the data
      var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
      var offset = 0;        // start at the beginning of the buffer
      gl.vertexAttribPointer(
        positionAttributeLocationPlain, size, type, normalize, stride, offset);

      // draw
      var primitiveType = gl.POINTS;
      var offset = 0;
      var count = 1;
      gl.drawArrays(primitiveType, offset, count);

    }

    else {
      webglUtils.resizeCanvasToDisplaySize(gl.canvas);

      // Tell WebGL how to convert from clip space to pixels
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      // Clear the canvas
      gl.clearColor(0, 0, 0, 0);
      gl.clear(gl.COLOR_BUFFER_BIT);

      // Tell it to use our program (pair of shaders)
      gl.useProgram(programTrans);

      // Turn on the attribute
      gl.enableVertexAttribArray(positionAttributeLocationTrans);

      // Bind the position buffer.
      gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferTrans);

      // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
      var size = 2;          // 2 components per iteration
      var type = gl.FLOAT;   // the data is 32bit floats
      var normalize = false; // don't normalize the data
      var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
      var offset = 0;        // start at the beginning of the buffer
      gl.vertexAttribPointer(
        positionAttributeLocationTrans, size, type, normalize, stride, offset);

      gl.uniformMatrix4fv(point_a_loc, false, new Float32Array(pointAM));
      gl.uniformMatrix4fv(point_b_loc, false, new Float32Array(pointBM));

      // draw
      var primitiveType = gl.TRIANGLES;
      var offset = 0;
      var count = 3;
      gl.drawArrays(primitiveType, offset, count);
    }
    // 4th

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferCircle);
    gl.bufferData(gl.ARRAY_BUFFER, vertexDataTyped, gl.STATIC_DRAW);
    webglUtils.resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    // Clear the canvas
    // gl.clearColor(0, 0, 0, 0);
    // gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(programCircle);
    gl.enableVertexAttribArray(positionAttributeLocationCircle);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBufferCircle);

    // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
    var size = 2;          // 2 components per iteration
    var type = gl.FLOAT;   // the data is 32bit floats
    var normalize = false; // don't normalize the data
    // var stride = ATTRIBUTES * Float32Array.BYTES_PER_ELEMENT;        // 0 = move forward size * sizeof(type) each iteration to get the next position
    var stride = 0;
    var offset = 0;        // start at the beginning of the buffer
    gl.vertexAttribPointer(
      positionAttributeLocationCircle, size, type, normalize, stride, offset);

    // draw
    var primitiveType = gl.LINES;
    var offset = 0;
    var count = vertexData.length/ATTRIBUTES;
    gl.drawArrays(primitiveType, offset, count);
  }

  // render();
  let isReflection = true;
  let translationButton = document.querySelector("#translations");
  translationButton.addEventListener("click", function (event) {
    console.log("clicked translation");
    isReflection = false;
    requestAnimationFrame(render);
  });

  let reflectionButton = document.querySelector("#reflections");
  reflectionButton.addEventListener("click", function (event) {
    console.log("clicked reflection");
    isReflection = true;
    requestAnimationFrame(render);
  });

  let resetButton = document.querySelector("#reset");
  resetButton.addEventListener("click", function (event) {
    console.log("clicked reset");
    reflectionPoint = [0.5, 0];
    reflectionPointM = pointToMatrix(reflectionPoint);

    pointA = [0,0];
    pointB = [0.0, 0];
    pointAM = pointToMatrix(pointA);
    pointBM = pointToMatrix(pointB);

    requestAnimationFrame(render);
  });

  requestAnimationFrame(render);

  let isDown = false;

  canvas.addEventListener('mousedown', function (event) {
    // check if mousedown is on the black square
    let x = event.clientX;
    let y = event.clientY;

    console.log("mouse down");


    let rect = event.target.getBoundingClientRect();
    console.log(rect);
    let offset_x = Math.abs(((0 - rect.left) - rect.width/2)/(rect.width/2) - ((5 - rect.left) - rect.width/2)/(rect.width/2));
    let offset_y = Math.abs((rect.height/2 - (0 - rect.top))/(rect.height/2) - (rect.height/2 - (5 - rect.top))/(rect.height/2));

    x = ((x - rect.left) - rect.width/2)/(rect.width/2);
    y = (rect.height/2 - (y - rect.top))/(rect.height/2);

    console.log(x);
    console.log(y);

    if (isReflection) {
      if ((Math.abs(x - reflectionPoint[0]) < offset_x) && Math.abs(y - reflectionPoint[1]) < offset_y) {
        console.log("in square");
        isDown = true;
      }
      else {
        isDown = false;
      }
    }

    else {
      isDown = true;
      if ((Math.abs(x - positions[0]) < offset_x) && Math.abs(y - positions[1]) < offset_y) {
        pointA = [positions[0], positions[1]];
        pointB = [0, 0];
        pointAM = pointToMatrix(pointA);
        pointBM = pointToMatrix(pointB);
        requestAnimationFrame(render);
      }
      if ((Math.abs(x - positions[2]) < offset_x) && Math.abs(y - positions[3]) < offset_y) {
        pointA = [positions[2], positions[3]];
        pointB = [0, 0];
        pointAM = pointToMatrix(pointA);
        pointBM = pointToMatrix(pointB);
        requestAnimationFrame(render);
      }
      if ((Math.abs(x - positions[4]) < offset_x) && Math.abs(y - positions[5]) < offset_y) {
        pointA = [positions[4], positions[5]];
        pointB = [0, 0];
        pointAM = pointToMatrix(pointA);
        pointBM = pointToMatrix(pointB);
        requestAnimationFrame(render);
      }
      else {
        pointA = [x, y];
        pointAM = pointToMatrix(pointA);
      }
    }
  });

  let x = 0;
  let y = 0;
  canvas.addEventListener('mousemove', function (event) {
    if (isDown) {
      x = event.clientX;
      y = event.clientY;

      let rect = event.target.getBoundingClientRect();
      x = ((x - rect.left) - rect.width/2)/(rect.width/2);
      y = (rect.height/2 - (y - rect.top))/(rect.height/2);

      console.log(x);
      console.log(y);

      if (isReflection) {
        reflectionPoint = [x, y];
      }

      else {
        pointB = [x, y];
        pointBM = pointToMatrix(pointB);
      }

      requestAnimationFrame(render);
    }
  });

  canvas.addEventListener('mouseup', function (event) {
    isDown = false;
  });

}

main();
